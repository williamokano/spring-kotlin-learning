package com.williamokano.repositories

import com.williamokano.models.Greeting
import org.springframework.stereotype.Repository
import java.util.concurrent.atomic.AtomicLong

@Repository
class GreetingRepository {
    companion object {
        val counter = AtomicLong()
    }

    fun getGreeting(name: String) = Greeting(counter.incrementAndGet(), name)
}