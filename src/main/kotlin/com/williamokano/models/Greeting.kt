package com.williamokano.models

data class Greeting(val id: Long, val content: String)