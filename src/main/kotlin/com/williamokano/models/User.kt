package com.williamokano.models

import javax.persistence.*

@Entity
@Table(name = "usuario")
data class User(
        @Id @GeneratedValue
        var idUsuario: Long,
        @Column(name = "id_usuario_tipo")
        var idTipoUsuario: Long,
        var idParceiro: Long?,
        var nomePessoa: String,
        var email: String
) {
    constructor() : this(1, 1, null, "", "")
}