package com.williamokano.controllers

import com.williamokano.models.Person
import com.williamokano.models.User
import com.williamokano.repositories.GreetingRepository
import com.williamokano.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class HomeController {

    @Autowired
    lateinit var userRepository: UserRepository;

    @GetMapping("/")
    fun greeting(
            @RequestParam(value = "name", defaultValue = "World") name: String,
            repository: GreetingRepository
    ) = repository.getGreeting(name)

    @GetMapping("/pessoa")
    fun pessoa() = Person("Okano", 28)

    @GetMapping("/teste")
    fun teste() : Iterable<User> {
        return userRepository.findAll()
    }
}